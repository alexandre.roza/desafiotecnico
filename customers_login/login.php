<?php     
    require_once('../config.php');
    require_once(DBAPI);

?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Página de login">
    <meta name="author" content="Alexandre Rosa">
    <link rel="icon" href="images/favicon.ico">

    <title>Desafio Técnico</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">   
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/jumbotron.css" rel="stylesheet">
  </head>

  <body>

    <div class="container panelLogin"> <!-- container -->

        <form class="form-signin" method="POST" action="validaLogin.php">
        <h2 class="form-signin-heading text-center">Efetuar Login</h2>
        <input type="text" name="usuario" class="form-control" placeholder="Digite seu usuário" required autofocus>
        
        <br />
        <input type="password" name="senha" class="form-control" placeholder="Digite sua senha" required>
        
        <button class="btn btn-lg btn-danger btn-block" type="submit">Acessar</button>
      </form>

    </div> <!-- /container -->
    
    <!-- Dependência jQuery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  </body>
</html>
