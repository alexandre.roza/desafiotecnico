SELECT nome_departamento, nome_completo, dias
FROM (
  SELECT funcionarios.*, depto.dept_nome as nome_departamento, from_date, (to_date - from_date) as dias
  FROM (
    SELECT emp_no,CONCAT(first, " ", last_name) AS nome_completo
    from employees
  ) AS funcionarios 
  join dept_emp as departamento on (departamento.emp_no = funcionarios.emp_no) 
  join departments as depto on (depto.dept_no = departamento.dept_no)
  where  to_date <= current_date
) AS resultado
order by  disas desc
LIMIT 0, 10;

mysqldump desafiotecnico_ars > c:\init.sql -v -h localhost -u root -pSENHA

mysqldump desafiotecnico_ars -u root  > c:\init.sql