<?php
    require_once('funcoes.php');
    index();
?>

<?php include(HEADER_TEMPLATE); ?>

<header>
	<div class="row">
		<div class="col-sm-6">
			<h2>Reserva</h2>
		</div>
		<div class="col-sm-6 text-right h2">
	    	<a class="btn btn-primary" href="add.php"><i class="fa fa-plus"></i> Novo Reserva</a>
	    	<a class="btn btn-default" href="index.php"><i class="fa fa-refresh"></i> Atualizar</a>
	    </div>
	</div>
</header>

<?php if (!empty($_SESSION['message'])) : ?>
	<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo $_SESSION['message']; ?>
	</div>
	<?php clear_messages(); ?>
<?php endif; ?>

<hr>

<table class="table table-hover">
<thead>
	<tr>
		<th>ID</th>
		<th>Sala</th>
		<th>Usuário</th>
		<th>Data</th>
		<th>Hora Inicial</th>
                <th>Hora Fim</th>
                <th>Finalizada</th>
		<th>Opções</th>
	</tr>
</thead>
<tbody>
<?php if ($customers) : ?>
<?php foreach ($customers as $customer) : ?>
	<tr>
            <td><?php echo $customer['id_reserva']; ?></td>
            <td>  
                <select class="form-control" name="customer['id_sala']" >
                   <?php  
                    foreach ($arrSala as $key => $value) { ?>
                    <option value="<?php echo $key; ?>" 
                        <?php echo $defineSelecao->verificaValorSelecionado($customer['id_sala'], $key); ?> > 
                        <?php echo $value; ?>
                    </option>
                   <?php } ?>
                </select>
            </td>
            <td>
                <select class="form-control" name="customer['id_usuario']" >
                   <?php  
                    foreach ($arrSala as $key => $value) { ?>
                    <option value="<?php echo $key; ?>" 
                        <?php echo $defineSelecao->verificaValorSelecionado($customer['id_usuario'], $key); ?> > 
                        <?php echo $value; ?>
                    </option>
                   <?php } ?>
                </select>
            </td>
            <td><?php echo $customer['data']; ?></td>
            <td><?php echo $customer['hora_ini']; ?></td>
            <td><?php echo $customer['hora_fim']; ?></td>
            <td><?php echo $customer['fechado']; ?></td>
            <td class="actions text-left">                    
                <a href="edit.php?id_reserva=<?php echo $customer['id_reserva']; ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Editar</a>
                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-modal" data-customer="<?php echo $customer['id_sala']; ?>">
                        <i class="fa fa-trash"></i> Excluir
                </a>
            </td>
	</tr>
<?php endforeach; ?>
<?php else : ?>
	<tr>
		<td colspan="6">Nenhum registro encontrado.</td>
	</tr>	
<?php endif; ?>
</tbody>
</table>
<?php include('modal.php'); ?>
<?php include(FOOTER_TEMPLATE); ?>