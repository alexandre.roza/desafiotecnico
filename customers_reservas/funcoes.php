<?php

require_once('../config.php');
require_once(DBAPI);

$customers = null;
$customer = null;

/**
 *  Listagem de salas
 */
function index() {
	global $customers;
	$customers = find_all('salas');
}

/**
 *  Cadastro de salas
 */
function add() {
  //var_dump($_POST);
  if (!empty($_POST['customer'])) {
    
    $today = date_create('now', new DateTimeZone('America/Sao_Paulo'));

    $customer = $_POST['customer'];
    //$customer['modified'] = $customer['created'] = $today->format("Y-m-d H:i:s");
    
    save('salas', $customer);
    header('location: index.php');
  }
}

/**
 *	Atualizacao/Edicao de salas
 */
function edit() {

  $now = date_create('now', new DateTimeZone('America/Sao_Paulo'));
  if (isset($_GET['id_sala'])) {

    $id = $_GET['id_sala'];

    if (isset($_POST['customer'])) {
      //var_dump($_POST);
      $customer = $_POST['customer'];
      //$customer['modified'] = $now->format("Y-m-d H:i:s");

      update('salas', $id, $customer);
      header('location: index.php');
    } else {

      global $customer;
      $customer = find('salas', $id);
    } 
  } else {
    header('location: index.php');
  }
}

/**
 *  Exclusão de um salas
 */
function delete($id = null) {

  global $customer;
  $customer = remove('salas', $id);

  header('location: index.php');
}
?>