<?php 
  require_once('funcoes.php'); 
  edit();
?>

<?php include(HEADER_TEMPLATE); ?>

<h2>Atualizar Sala</h2>

<form action="edit.php?id_sala=<?php echo $customer['id_sala']; ?>" method="post">
  <hr />
  <div class="row">
    <div class="form-group col-md-7">
      <label for="name">Descrição da Sala</label>
      <input type="text" class="form-control" name="customer['descricao']" value="<?php echo $customer['descricao']; ?>">
    </div>

    <!--<<div class="form-group col-md-3">
      <label for="campo2">Hora Inicial da Reserva</label>
      <input type="password" class="form-control" name="customer['hora_inicio_reserva']" value="<?php //echo $customer['senha']; ?>">
    </div>-->
  <div id="actions" class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-primary">Salvar</button>
      <a href="index.php" class="btn btn-default">Cancelar</a>
    </div>
  </div>
</form>

<?php include(FOOTER_TEMPLATE); ?>