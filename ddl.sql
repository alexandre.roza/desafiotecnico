CREATE TABLE reserva (
  id_reserva INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  id_usario INTEGER UNSIGNED NOT NULL,
  id_sala INTEGER UNSIGNED NOT NULL,
  data_reserva_inicial DATE NULL,
  data_reserva_final DATE NULL,
  hora_reserva_inicial TIME NULL,
  hora_reserva_final TIME NULL,
  PRIMARY KEY(id_reserva, id_usario, id_sala)
);

CREATE TABLE salas (
  id_sala INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  desc_sala INTEGER UNSIGNED NULL,
  hora_inicio_reserva TIME NULL,
  PRIMARY KEY(id_sala)
);

CREATE TABLE usuario (
  id_usuario INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome_usuario VARCHAR(20) NULL,
  PRIMARY KEY(id_usuario)
);


