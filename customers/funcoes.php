<?php

require_once('../config.php');
require_once(DBAPI);

$customers = null;
$customer = null;

/**
 *  Listagem de usuario
 */
function index() {
	global $customers;
	$customers = find_all('usuario');
}

/**
 *  Cadastro de usuraio
 */
function add() {

  if (!empty($_POST['customer'])) {
    
    $today = date_create('now', new DateTimeZone('America/Sao_Paulo'));

    $customer = $_POST['customer'];
    //$customer['modified'] = $customer['created'] = $today->format("Y-m-d H:i:s");
    
    save('usuario', $customer);
    header('location: index.php');
  }
}

/**
 *	Atualizacao/Edicao de Cliente
 */
function edit() {

  $now = date_create('now', new DateTimeZone('America/Sao_Paulo'));
  if (isset($_GET['id_usuario'])) {

    $id = $_GET['id_usuario'];

    if (isset($_POST['customer'])) {

      $customer = $_POST['customer'];
      //$customer['modified'] = $now->format("Y-m-d H:i:s");

      update('usuario', $id, $customer);
      header('location: index.php');
    } else {

      global $customer;
      $customer = find('usuario', $id);
    } 
  } else {
    header('location: index.php');
  }
}

/**
 *  Exclusão de um Cliente
 */
function delete($id = null) {

  global $customer;
  $customer = remove('usuario', $id);

  header('location: index.php');
}
?>