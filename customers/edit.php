<?php 
  require_once('funcoes.php'); 
  edit();
?>

<?php include(HEADER_TEMPLATE); ?>

<h2>Atualizar Usuário</h2>

<form action="edit.php?id_usuario=<?php echo $customer['id_usuario']; ?>" method="post">
  <hr />
  <div class="row">
    <div class="form-group col-md-7">
      <label for="name">Nome Usuário</label>
      <input type="text" class="form-control" name="customer['nome_usuario']" value="<?php echo $customer['nome_usuario']; ?>">
    </div>

    <div class="form-group col-md-3">
      <label for="campo2">Sanha</label>
      <input type="password" class="form-control" name="customer['senha']" value="<?php echo $customer['senha']; ?>">
    </div>
  <div id="actions" class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-primary">Salvar</button>
      <a href="index.php" class="btn btn-default">Cancelar</a>
    </div>
  </div>
</form>

<?php include(FOOTER_TEMPLATE); ?>