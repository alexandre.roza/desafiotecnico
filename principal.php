<?php require_once 'config.php'; ?>
<?php require_once DBAPI; ?>

<?php require_once 'config.php'; ?>
<?php require_once DBAPI; ?>

<?php require_once(HEADER_TEMPLATE); ?>

<?php $db = open_database(); ?>

<h1>Reservar Salas</h1>
<hr />

<?php if ($db) : ?>

<div class="row">
	<div class="col-xs-6 col-sm-3 col-md-2">
		<a href="customers_reservas/add.php" class="btn btn-primary">
			<div class="row">
				<div class="col-xs-12 text-center">
					<i class="fa fa-plus fa-5x"></i>
				</div>
				<div class="col-xs-12 text-center">
					<p>Reservar Salas</p>
				</div>
			</div>
		</a>
	</div>
</div>
<br>	
	<hr>

<table class="table table-hover">
<thead>
	<tr>
		<th width="30%">Nome Usuario</th>
		<!--<th>CPF/CNPJ</th>
		<th>Telefone</th>-->
		<th>Sala</th>
		<th>Data</th>
		<th>Hora Inicial</th>
		<th>Hora Final</th>
		<th>Opções</th>
	</tr>
</thead>
<tbody>

	<tr>
		<td><?php //echo $customer['id_usuario']; ?></td>
		<td><?php //echo $customer['nome_usuario']; ?></td>
		<td><?php //echo $customer['nome_usuario']; ?></td>
		<td><?php //echo $customer['nome_usuario']; ?></td>
		<td><?php //echo $customer['nome_usuario']; ?></td>
		<td><?php //echo $customer['nome_usuario']; ?></td>
		<td class="actions text-left">
			<!--<a href="view.php?id=<?php// echo $customer['id_usuario']; ?>" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> Visualizar</a>-->
			<a href="edit.php?id_usuario=<?php //echo $customer['id_usuario']; ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Editar</a>
			<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-modal" data-customer="<?php //echo $customer['id_usuario']; ?>">
				<i class="fa fa-trash"></i> Excluir
			</a>
		</td>
	</tr>

</tbody>
</table>

	<!--<div class="col-xs-6 col-sm-3 col-md-2">
		<a href="customers" class="btn btn-default">
			<div class="row">
				<div class="col-xs-12 text-center">
					<i class="fa fa-user fa-5x"></i>
				</div>
				<div class="col-xs-12 text-center">
					<p>Clientes</p>
				</div>
			</div>
		</a>
	</div>-->
</div>

<?php else : ?>
	<div class="alert alert-danger" role="alert">
		<p><strong>ERRO:</strong> Não foi possível Conectar ao Banco de Dados!</p>
	</div>

<?php endif; ?>

<?php include(FOOTER_TEMPLATE); ?>